<?php


/**
* Display help and module information
* @param path which path of the site we're displaying help
* @param arg array that holds the current path as would be returned from arg() function
* @return help text for the path
*/
function zscore_help($path, $arg) {
  $output = '';  //declare your output variable
  switch ($path) {
    case "admin/help#zscore":
      $output = '<p>'.  t("Finding ZScore of an Individual. If your ZScore is 0 or close  to it, you are fine. The deviation from 0 would mean that the growth is not normal.") .'</p>';
      break;
  }
  return $output;
}

/**
* Implementation of hook_menu().
*/
function zscore_menu() {
  $items = array();

  $items['zscore'] = array(
  'title' => 'ZScore ',
  //'description' => 'ZScore .',
  'page callback' => 'zscore_page',
  //'page arguments' => 'zscore_parameters_form',
  'access arguments' => array('zscores configurations'),
  );

  return $items;
}

function zscore_page() {

    return drupal_get_form('zscore_parameters_form');
}

/**
 * Implements hook_permission().
 */
function zscore_permission() {
  $perms = array(
    'zscores configurations' => array(
      'title' => t('View and Generate ZScore'),
    ),
  );

  return $perms;
}


/**
* Define a form.
*/
function zscore_parameters_form($form, &$form_state) {

  $form['zscore'] = array(
    '#title' => t('ZScore Parameters'),
    '#type' => 'fieldset',
    '#description' => t('Get ZScore of the individual'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['zscore']['weight'] = array(
    '#title' => t('Weight'),
    '#type' => 'textfield',
   // '#default_value'=> variable_get("inst_name_full"),
    '#description' => t('Weight of the individual'),
  );

  $form['zscore']['height'] = array(
    '#title' => t('Height'),
    '#type' => 'textfield',
   // '#default_value'=> variable_get("inst_name_full"),
    '#description' => t('Height of the individual'),
  );

  $form['zscore']['gender'] = array(
    '#title' => t('Gender'),
    '#type' => 'select',
    '#options' => drupal_map_assoc(array('MALE','FEMALE')),
    '#required' => TRUE,
   // '#default_value'=> variable_get("inst_name_full"),
    '#description' => t('Gender of the individual'),
  );

  $form['zscore']['dob'] = array(
    '#title' => t('Date of Birth'),
    '#type' => 'date_popup',
    '#date_format' => 'Y-m-d',
    '#date_label_position' => 'inline',
    '#required' => TRUE,
    //'#date_year_range' => '-19+0',
   // '#default_value'=> variable_get("inst_name_full"),
    '#description' => t('Date of Birth of the individual'),
  );

  $form['submit']=array('#type'=>'submit','#value'=>'Submit');

  if(!isset($_SESSION["zscores"])){
    $_SESSION['zscores']="";
  }else{
    $form['zscore']['#collapsed'] = TRUE;
  }
  $form['session_zscores'] = array(
    '#type' => 'item',
    '#markup' =>$_SESSION["zscores"],
  );
 $_SESSION["zscores"]=null;
  return $form;
}


function zscore_parameters_form_validate($form, &$form_state) {
}

function zscore_parameters_form_submit($form, &$form_state) {
  $param =array();
  $param['weight']=$form_state['values']['weight'];
  $param['height']=$form_state['values']['height'];
  $param['dob']=$form_state['values']['dob'];
  $param['gender']=$form_state['values']['gender'];

  // Remember values
  $form_state['storage']['remember_weight'] = $form_state['values']['weight'];
  $form_state['storage']['remember_height'] = $form_state['values']['height'];
  $form_state['storage']['remember_dob'] = $form_state['values']['dob'];
  $form_state['storage']['remember_gender'] = $form_state['values']['gender'];
  // Rebuild the form
  $form_state['rebuild'] = TRUE;
  $zscore = zscore_zscore($param);
  //drupal_set_message("ZScore is:<pre>".print_r($zscore,1)."</pre>");
  $zscore_final = "<strong>BMI For Age: </strong>: ".$zscore['bfa'];
  $zscore_final.= "<br/><strong>Weight For Age: </strong>: ".$zscore['wfa'];
  $zscore_final.= "<br/><strong>Height For Age: </strong>: ".$zscore['hfa'];
  $zscore_final.= "<br/><strong>Weight For Height: </strong>: ".$zscore['wfh'];
  $_SESSION["zscores"]= $zscore_final;
}


/*
* Gets data from entity to provide ZScore. To change it to an admin page for ZScore
* Submission
*/
function zscore_zscore(&$param){
  $path = drupal_get_path('module', 'zscore');
  //drupal_set_message("Path: $path");

  $gender = $param['gender'];
  if($gender == "MALE"){
    $gender = "boys";
  }else{
    $gender = "girls";
  }
  $param['gender']=$gender;

  $dob = $param['dob'];
  $dob = new DateTime($param['dob']);
  $visit = new DateTime(date('Y-m-d'));
  $diff = $visit->diff($dob);
  //drupal_set_message("Age: Year: ".$diff->y." Month: ".$diff->m." days: ". $diff->d);
  $age= NULL;
  if($diff->y >= 5){
    $age = round(($diff->y*12 + $diff->m + ($diff->d/30)),0);
    $param['age_unit']='month';
  }else{
    $age = round(($diff->y*12*30.4375 + $diff->m*30.4375+$diff->d),0);
    $param['age_unit']='days';
  }
  $param['age']=$age;
  //drupal_set_message("Age is: $age Age unit is: ".$param['age_unit']);
  $zscore  = zscore_get_zscore($param);
  return $zscore;
}

function zscore_get_zscore($param){
  //drupal_set_message("BAse Path: ".base_path());
  $path = drupal_get_path('module', 'zscore');
  //drupal_set_message("Path: $path");
  include_once($path.'/zscores/ZScore.php');
  $score = new ZScore();
  $score->set_gender($param['gender']);
  $score->set_base_path($_SERVER['DOCUMENT_ROOT'] . base_path().$path.'/zscores');
  if(!empty($param['weight'])){
    $score->set_weight($param['weight']);
  }
  if(!empty($param['height'])){
    $score->set_height($param['height']);
  }
  if(!empty($param['length'])){
    $score->set_length($param['length']);
  }
  if(!empty($param['age']) && !empty($param['age_unit'])){
    $score->set_age($param['age'],$param['age_unit']);
  }

  $z_score=array(ZScore::BMI_FOR_AGE=>'',ZScore::WEIGHT_FOR_AGE=>'',ZScore::HEIGHT_FOR_AGE=>'',ZScore::WEIGHT_FOR_HEIGHT=>'');

  if(isset($param['weight']) && isset($param['height']) && isset($param['age'])) {
    $score->set_index(ZScore::BMI_FOR_AGE);
    $lms = $score->get_lms();
    if(zscore_isvalidlms($lms,$score->get_index())){
      $zscore = $score->get_zind($lms);
      $z_score[ZScore::BMI_FOR_AGE]= "$zscore";
    }else{
      $z_score[ZScore::BMI_FOR_AGE]= "N/A";
    }
  }else{
    $z_score[ZScore::BMI_FOR_AGE]= "N/A";
  }

  if(isset($param['weight']) && isset($param['age'])) {
    $score->set_index(ZScore::WEIGHT_FOR_AGE);
    $lms = $score->get_lms();
    if(zscore_isvalidlms($lms,$score->get_index())){
      $zscore = $score->get_zind($lms);
      $z_score[ZScore::WEIGHT_FOR_AGE]= "$zscore";
    }else{
      $z_score[ZScore::WEIGHT_FOR_AGE]= "N/A";
    }
  }else{
    $z_score[ZScore::WEIGHT_FOR_AGE]= "N/A";
  }

  if(isset($param['height']) && isset($param['age'])) {
    $score->set_index(ZScore::HEIGHT_FOR_AGE);
    $lms = $score->get_lms();
    if(zscore_isvalidlms($lms,$score->get_index())){
      $zscore = $score->get_zind($lms);
      $z_score[ZScore::HEIGHT_FOR_AGE]= "$zscore  ";
    }else{
      $z_score[ZScore::HEIGHT_FOR_AGE]= "N/A";
    }
  }else{
    $z_score[ZScore::HEIGHT_FOR_AGE]= "N/A";
  }

  if(isset($param['height']) && isset($param['weight']) && $score->get_age_group() == '0_5') {
    $score->set_index(ZScore::WEIGHT_FOR_HEIGHT);
    $lms = $score->get_lms();
    if(zscore_isvalidlms($lms,$score->get_index())){
      $zscore = $score->get_zind($lms);
      $z_score[ZScore::WEIGHT_FOR_HEIGHT]= "$zscore";
    }else{
      $z_score[ZScore::WEIGHT_FOR_HEIGHT]= "N/A";
    }
  }else{
    $z_score[ZScore::WEIGHT_FOR_HEIGHT]= "N/A";
  }

  return $z_score;
}

function zscore_isvalidlms($lms,$indicator){
  if(empty($lms['L']) || $lms['L'] == 0){
    if($indicator == ZScore::BMI_FOR_AGE || $indicator == ZScore::HEIGHT_FOR_AGE || $indicator == ZScore::WEIGHT_FOR_AGE){
      drupal_set_message(t('Either the age is not present or is out of bounds'), 'warning');
    }else if($indicator == ZScore::WEIGHT_FOR_HEIGHT){
      drupal_set_message(t('Either the height/length is not present or is out of bounds'), 'warning');
    }
    return FALSE;
  }
  return TRUE;
}




?>


